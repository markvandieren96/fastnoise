use super::*;
use mcnum::Vec3f;

pub struct Scaleable<F> {
	pub noise: F,
	scale: f32,
	bias: f32,
	point_scale: Vec3f,
}

impl<F> Scaleable<F> {
	pub fn new(noise: F, scale: f32, bias: f32, point_scale: Vec3f) -> Self {
		Self { noise, scale, bias, point_scale }
	}
}

impl<F> NoiseFn2D for Scaleable<F>
where
	F: NoiseFn2D,
{
	fn get_2d(&self, p: Vec2f) -> f32 {
		self.noise.get_2d(self.point_scale.xy() * p) * self.scale + self.bias
	}
}

impl<F> NoiseFn3D for Scaleable<F>
where
	F: NoiseFn3D,
{
	fn get_3d(&self, p: Vec3f) -> f32 {
		self.noise.get_3d(self.point_scale * p) * self.scale + self.bias
	}
}

impl<F> Scaleable<F>
where
	F: NoiseFn2D,
{
	pub fn sample_range_2d(&self) -> (f32, f32) {
		let mut min = std::f32::MAX;
		let mut max = std::f32::MIN;

		for x in 0..1024 {
			for y in 0..1024 {
				let f = self.get_2d(Vec2::new(x as f32, y as f32));
				if f < min {
					min = f;
				}
				if f > max {
					max = f;
				}
			}
		}

		(min, max)
	}
}
