use mcnum::*;

mod fastnoise;
mod scaleable;

pub use self::fastnoise::*;
pub use scaleable::*;

pub trait NoiseFn2D {
	fn get_2d(&self, p: Vec2f) -> f32;
}

pub trait NoiseFn3D {
	fn get_3d(&self, p: Vec3f) -> f32;
}
