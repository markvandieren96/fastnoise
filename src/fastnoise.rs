use super::*;
use mcnum::{Vec2, Vec2f, Vec3f};
use std::convert::TryFrom;

extern "C" {
	fn fn_create() -> *const libc::c_void;
	fn fn_destroy(p: *const libc::c_void);

	fn fn_set_seed(p: *const libc::c_void, seed: i32);
	fn fn_set_frequency(p: *const libc::c_void, frequency: f32);
	fn fn_set_type(p: *const libc::c_void, noise_type: i32);
	fn fn_set_interp(p: *const libc::c_void, interp_type: i32);
	fn fn_set_fractal_octaves(p: *const libc::c_void, octaves: i32);
	fn fn_set_fractal_lacunarity(p: *const libc::c_void, lacunarity: f32);
	fn fn_set_fractal_gain(p: *const libc::c_void, gain: f32);
	fn fn_set_fractal_type(p: *const libc::c_void, fractal_type: i32);

	fn fn_get_2d(p: *const libc::c_void, x: f32, y: f32) -> f32;
	fn fn_get_3d(p: *const libc::c_void, x: f32, y: f32, z: f32) -> f32;
}

unsafe impl Send for FastNoise {}
unsafe impl Sync for FastNoise {}

pub enum NoiseType {
	Value = 0,
	ValueFractal = 1,
	Perlin = 2,
	PerlinFractal = 3,
	Simplex = 4,
	SimplexFractal = 5,
	Cellular = 6,
	WhiteNoise = 7,
	Cubic = 8,
	CubicFractal = 9,
}

impl TryFrom<&str> for NoiseType {
	type Error = ();

	fn try_from(s: &str) -> Result<Self, ()> {
		match s {
			"Value" => Ok(NoiseType::Value),
			"ValueFractal" => Ok(NoiseType::ValueFractal),
			"Perlin" => Ok(NoiseType::Perlin),
			"PerlinFractal" => Ok(NoiseType::PerlinFractal),
			"Simplex" => Ok(NoiseType::Simplex),
			"SimplexFractal" => Ok(NoiseType::SimplexFractal),
			"Cellular" => Ok(NoiseType::Cellular),
			"WhiteNoise" => Ok(NoiseType::WhiteNoise),
			"Cubic" => Ok(NoiseType::Cubic),
			"CubicFractal" => Ok(NoiseType::CubicFractal),
			_ => Err(()),
		}
	}
}

pub enum Interp {
	Linear = 0,
	Hermite = 1,
	Quintic = 2,
}

impl TryFrom<&str> for Interp {
	type Error = ();

	fn try_from(s: &str) -> Result<Self, ()> {
		match s {
			"Linear" => Ok(Interp::Linear),
			"Hermite" => Ok(Interp::Hermite),
			"Quintic" => Ok(Interp::Quintic),
			_ => Err(()),
		}
	}
}

pub enum FractalType {
	FBM = 0,
	Billow = 1,
	RigidMulti = 2,
}

impl TryFrom<&str> for FractalType {
	type Error = ();

	fn try_from(s: &str) -> Result<Self, ()> {
		match s {
			"FBM" => Ok(FractalType::FBM),
			"Billow" => Ok(FractalType::Billow),
			"RigidMulti" => Ok(FractalType::RigidMulti),
			_ => Err(()),
		}
	}
}

pub struct FastNoise {
	p: *const libc::c_void,
	scale2: f32,
	bias2: f32,
}

impl FastNoise {
	pub fn new(seed: u64, frequency: f32, normalize: bool) -> FastNoise {
		let n = FastNoise {
			p: unsafe { fn_create() },
			scale2: if normalize { 0.5 } else { 1.0 },
			bias2: if normalize { 0.5 } else { 0.0 },
		};
		n.set_seed(seed as i32);
		n.set_frequency(frequency);
		n
	}

	pub fn set_seed(&self, seed: i32) {
		unsafe { fn_set_seed(self.p, seed) };
	}

	pub fn set_frequency(&self, frequency: f32) {
		unsafe { fn_set_frequency(self.p, frequency) };
	}

	pub fn set_type(&self, noise_type: NoiseType) {
		unsafe { fn_set_type(self.p, noise_type as i32) };
	}
	pub fn set_interp(&self, interp_type: Interp) {
		unsafe { fn_set_interp(self.p, interp_type as i32) };
	}
	pub fn set_fractal_octaves(&self, octaves: i32) {
		unsafe { fn_set_fractal_octaves(self.p, octaves) };
	}
	pub fn set_fractal_lacunarity(&self, lacunarity: f32) {
		unsafe { fn_set_fractal_lacunarity(self.p, lacunarity) };
	}
	pub fn set_fractal_gain(&self, gain: f32) {
		unsafe { fn_set_fractal_gain(self.p, gain) };
	}
	pub fn set_fractal_type(&self, fractal_type: FractalType) {
		unsafe { fn_set_fractal_type(self.p, fractal_type as i32) };
	}

	pub fn get_2d(&self, pos: Vec2f) -> f32 {
		unsafe { fn_get_2d(self.p, pos.x, pos.y) * self.scale2 + self.bias2 }
	}

	pub fn get_3d(&self, pos: Vec3f) -> f32 {
		unsafe { fn_get_3d(self.p, pos.x, pos.y, pos.z) * self.scale2 + self.bias2 }
	}

	pub fn sample_range(&self) -> (f32, f32) {
		let mut min = std::f32::MAX;
		let mut max = std::f32::MIN;

		for x in 0..1024 {
			for y in 0..1024 {
				let f = self.get_2d(Vec2::new(x, y).into());
				if f < min {
					min = f;
				}
				if f > max {
					max = f;
				}
			}
		}

		(min, max)
	}
}

impl Drop for FastNoise {
	fn drop(&mut self) {
		unsafe { fn_destroy(self.p) };
	}
}

impl NoiseFn2D for FastNoise {
	fn get_2d(&self, p: Vec2f) -> f32 {
		self.get_2d(p)
	}
}

impl NoiseFn3D for FastNoise {
	fn get_3d(&self, p: Vec3f) -> f32 {
		self.get_3d(p)
	}
}
