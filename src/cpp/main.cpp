#include "FastNoise.h"
#include <cstdint>

#if defined(_MSC_VER)
//  Microsoft 
#define EXPORT __declspec(dllexport)
#define IMPORT __declspec(dllimport)
#elif defined(__GNUC__)
//  GCC
#define EXPORT __attribute__((visibility("default")))
#define IMPORT
#else
//  do nothing and hope for the best?
#define EXPORT
#define IMPORT
#pragma warning Unknown dynamic link import/export semantics.
#endif

extern "C" EXPORT void* fn_create();
extern "C" EXPORT void fn_destroy(void* noise);

extern "C" EXPORT void fn_set_seed(void* noise, int32_t seed);
extern "C" EXPORT void fn_set_frequency(void* noise, float frequency);
extern "C" EXPORT void fn_set_type(void* noise, int32_t noise_type);
extern "C" EXPORT void fn_set_interp(void* noise, int32_t interp_type);
extern "C" EXPORT void fn_set_fractal_octaves(void* noise, int32_t octaves);
extern "C" EXPORT void fn_set_fractal_lacunarity(void* noise, float lacunarity);
extern "C" EXPORT void fn_set_fractal_gain(void* noise, float gain);
extern "C" EXPORT void fn_set_fractal_type(void* noise, int32_t fractal_type);

extern "C" EXPORT float fn_get_2d(void* noise, float x, float y);
extern "C" EXPORT float fn_get_3d(void* noise, float x, float y, float z);

FastNoise* cast(void* noise)
{
	return reinterpret_cast<FastNoise*>(noise);
}

void* fn_create()
{
	auto noise = new FastNoise;
	noise->SetNoiseType(FastNoise::Simplex);
	return noise;
}

void fn_destroy(void* noise)
{
	const auto n = cast(noise);
	delete n;
}

void fn_set_seed(void* noise, int32_t seed)
{
	auto n = cast(noise);
	n->SetSeed(seed);
}

void fn_set_frequency(void* noise, float frequency)
{
	auto n = cast(noise);
	n->SetFrequency(frequency);
}

void fn_set_type(void * noise, int32_t noise_type)
{
	auto n = cast(noise);
	n->SetNoiseType(static_cast<FastNoise::NoiseType>(noise_type));
}

void fn_set_interp(void* noise, int32_t interp_type)
{
	auto n = cast(noise);
	n->SetInterp(static_cast<FastNoise::Interp>(interp_type));
}

void fn_set_fractal_octaves(void* noise, int32_t octaves)
{
	auto n = cast(noise);
	n->SetFractalOctaves(octaves);
}

void fn_set_fractal_lacunarity(void* noise, float lacunarity)
{
	auto n = cast(noise);
	n->SetFractalLacunarity(lacunarity);
}

void fn_set_fractal_gain(void* noise, float gain)
{
	auto n = cast(noise);
	n->SetFractalGain(gain);
}

void fn_set_fractal_type(void* noise, int32_t fractal_type)
{
	auto n = cast(noise);
	n->SetFractalType(static_cast<FastNoise::FractalType>(fractal_type));
}

float fn_get_2d(void* noise, float x, float y)
{
	const auto n = cast(noise);
	return n->GetNoise(x, y);
}

float fn_get_3d(void* noise, float x, float y, float z)
{
	const auto n = cast(noise);
	return n->GetNoise(x, y, z);
}
