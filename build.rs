extern crate cc;

fn main() {
	cc::Build::new().file("src/cpp/FastNoise.cpp").warnings(false).file("src/cpp/main.cpp").compile("FastNoiseWrap");
	if !cfg!(target_os = "windows") {
		println!("cargo:rustc-link-lib=stdc++");
	}
}
